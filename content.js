var statisticsManager = null;
var API_HOST = "https://hivetaxi.ortoped.org.ru";
var resultCallbackFunction = null;


$(document).ready(function() {
	console.log("content js document ready");
//	$('head').append("<script type='text/javascript' src='https://hivetaxi.ortoped.org.ru/static/hivetaxi-backend.js'>");
	statisticsManager = new StatisticsManager();
	var div = document.createElement("div");
	$("body").get(0).appendChild(div);
	$(div).attr("id", "statisticsDialog");
	var statisticsDialog = $(div).dialog({
		autoOpen: false,
		height: 600,
		width: 800,
		modal: true,
		buttons: {
			"Закрыть": function() {
				statisticsDialog.dialog( "close" );
			}
		},
	});
	checkYandexButton();
});


function addExtensionToolbarLinks() {
	console.log("addExtensionToolbarLinks");
	if(!$(".help-btn").length) {
		console.log("no help btn");
		return;
	}
	var div = document.createElement("div");
	$(div).attr("id", "extensionToolbar");
	var statisticsLink = document.createElement("a");
	$(statisticsLink).attr("href", "#");
	$(statisticsLink).attr("style", "font-size: 10px; color: white; display: block; margin-bottom: 20px;");
	$(statisticsLink).html("Статистика передач в Яндекс");
	$(statisticsLink).click(function() {
		showStatistics();
		return false;
	});
	div.appendChild(statisticsLink);
	var referenceNode = $("sideline").find(".help-btn").get(0);
	console.log("referenceNode:");
	console.log(referenceNode);
	referenceNode.parentNode.insertBefore(div, referenceNode.nextSibling);
}

function showStatistics() {
	console.log("showStatistics");
	$("#statisticsDialog").dialog("open");
	var statisticsDialog = $("#statisticsDialog").get(0);
	while(statisticsDialog.hasChildNodes()) {
		statisticsDialog.removeChild(statisticsDialog.firstChild);
    }
	var table = document.createElement("table");
	$(table).attr("border", "1");
	$(table).attr("width", "100%");
	statisticsDialog.appendChild(table);
	var tr = document.createElement("tr");
	var th = document.createElement("th");
	$(th).html("Время");
	tr.appendChild(th);
	th = document.createElement("th");
	$(th).html("Статус заказа");
	tr.appendChild(th);
	th = document.createElement("th");
	$(th).html("Телефон");
	tr.appendChild(th);
	th = document.createElement("th");
	$(th).html("Куда подать");
	tr.appendChild(th);
	th = document.createElement("th");
	$(th).html("Куда поедут");
	tr.appendChild(th);
	table.appendChild(tr);
	var statistics = statisticsManager.getStatistics();
	console.log(statistics);
	report = "Время;Статус заказа;Телефон;Куда подать;Куда поедут\n";
	for(var index in statistics) {
		var record = statistics[index];
		var tr = document.createElement("tr");
		var td = document.createElement("td");
		var dateStr = formatDate(record.getTimestamp());
		report = report + dateStr + ";";
		$(td).html(dateStr);
		tr.appendChild(td);
		td = document.createElement("td");
		var status = record.getOrderStatus();
		var statusStr = status == "new" ? "Новый" : "Создан";
		report = report + statusStr + ";";
		$(td).html(statusStr);
		tr.appendChild(td);
		td = document.createElement("td");
		$(td).html(record.getIncomingPhone());
		tr.appendChild(td);
		report = report + record.getIncomingPhone() + ";";
		td = document.createElement("td");
		$(td).html(record.getIncomingAddress());
		tr.appendChild(td);
		report = report + record.getIncomingAddress() + ";";
		td = document.createElement("td");
		$(td).html(record.getDestinationAddress());
		tr.appendChild(td);
		report = report + record.getDestinationAddress() + "\n";
		table.appendChild(tr);
	}
	var link = document.createElement("a");
	$(link).attr("style", "display: block; margin-top: 20px; text-decoration: underline;");
	$(link).attr("href", 'data:text/csv;charset=utf-8,' + encodeURIComponent(report));
	$(link).html("Сохранить в виде файла CSV...");
	statisticsDialog.appendChild(link);
}


function formatDate(timestamp) {
	var date = new Date(timestamp);
	var dateStr = date.getDate() + "";
	if(dateStr.length < 2) {
		dateStr = "0" + dateStr;
	}
	var monthStr = (date.getMonth() + 1) + "";
	if(monthStr.length < 2) {
		monthStr = "0" + monthStr;
	}
	var hourStr = date.getHours() + "";
	if(hourStr.length < 2) {
		hourStr = "0" + hourStr;
	}
	var minuteStr = date.getMinutes() + "";
	if(minuteStr.length < 2) {
		minuteStr = "0" + minuteStr;
	}
	var secondStr = date.getSeconds() + "";
	if(secondStr.length < 2) {
		secondStr = "0" + secondStr;
	}
	return dateStr + "." + monthStr + "." + date.getFullYear() + " " + hourStr + ":" + minuteStr + ":" + secondStr;
}


function checkYandexButton() {
	var timerId = setInterval(function() {
		$(".s-button").find("span").each(function() {
			if($(this).html() == "ПРИНЯТЬ") {
//				console.log("found");
//				console.log($(this));
				var parent = $(this).parent().parent();
//				console.log(parent);
				var yandexButtons = parent.find("#btnSendToYandex");
				if(!yandexButtons.length) {
					popupAddYandexButton(parent);
				}
				if(!popupCheckPreorder() && !popupIsMultipleDestination() && popupGetIncomingPhone()) {
					$("#btnSendToYandex").removeAttr("disabled");
				} else {
					$("#btnSendToYandex").attr("disabled", "true");
				}
			}
		});
		if(!$("#extensionToolbar").length) {
			addExtensionToolbarLinks();
		}
		if(!$(".right-panel").find(".order-notification").parent().find("#btnCancelAndSendToYandex").length) {
			addExistingOrderSendToYandexButton();
		} else {
			if(!isArchivePage() && !isExistingOrderPreorder() && !isExistingOrderMultipleDestination() && !isExistingOrderCarAssigned()) {
				$("#btnCancelAndSendToYandex").removeAttr("disabled");
			} else {
				$("#btnCancelAndSendToYandex").attr("disabled", "true");
			}
		}
		if(isArchivePage()) {
			$("#btnCancelAndSendToYandex").hide();
		} else {
			$("#btnCancelAndSendToYandex").show();
		}
	}, 1000);		
}


function isArchivePage() {
	var url = window.location.href;
	if(url.indexOf("/archive") > -1) {
		return true;
	}
	return false;
}


function isExistingOrderMultipleDestination() {
	var length = $(".right-panel").find(".order-notification").parent().parent().find(".-light").find(".icon-with-text").length;
//	console.log("isExistingOrderMultipleDestination: " + length);
	return length > 2;
}


function existingOrderGetIncomingAddress() {
	var address = "";
	var select = $(".right-panel").find(".order-notification").parent().parent().find(".-light").find(".icon-with-text");
	if(select.length) {
		address = $(select.get(0)).find("span").html();
	}
	return address;
}


function isExistingOrderCarAssigned() {
	var length1 = $(".right-panel").find(".order-notification").parent().parent().find(".-car-assigned").length;	
	var length2 = $(".right-panel").find(".order-notification").parent().parent().find(".-car-waiting").length;	
	var length3 = $(".right-panel").find(".order-notification").parent().parent().find(".driver-xxs").length;	
	return length1 > 0 || length2 > 0 || length3 > 0;
}


function existingOrderGetDestinationAddress() {
	var address = "";
	var select = $(".right-panel").find(".order-notification").parent().parent().find(".-light").find(".icon-with-text");
	if(select.length > 1) {
		address = $(select.get(1)).find(".md-normal").html();
	}
	return address;
}


function existingOrderGetIncomingPhone() {
	var number = $(".right-panel").find(".flex-component_grow").find(".md-bold").html();
	if(!number) {
		return "";
	}
	number = number.replace( /[^0-9]/g, '' );
//	console.log("after replace: " + number);
	if(number.startsWith("8")) {
		number = "7" + number.substring(1, number.length);
	}
	number = "+" + number;
	return number;
}


function strip_tags( str ){	// Strip HTML and PHP tags from a string
	// 
	// +   original by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
	return str.replace(/<\/?[^>]+>/gi, '');
}


function addExistingOrderSendToYandexButton() {
	if($(".right-panel").find(".order-notification").length) {
		var button = document.createElement("button");
		$(button).attr("id", "btnCancelAndSendToYandex");
		$(button).addClass("s-button");
		$(button).addClass("-std");
		$(button).addClass("-normal");
		var span = document.createElement("span");
		$(span).html("Передать в Яндекс");
		button.appendChild(span);
		$(".right-panel").find(".order-notification").parent().get(0).appendChild(button);
		$(button).click(function() {
			var incomingPhone = existingOrderGetIncomingPhone();
			console.log("incomingPhone = " + incomingPhone);
			var incomingAddress = existingOrderGetIncomingAddress();
			console.log("incomingAddress = " + incomingAddress);
			var destinationAddress = existingOrderGetDestinationAddress();
			console.log("destinationAddress = " + destinationAddress);
			newWindow = window.open("https://taxi.yandex.ru/partner-orders/?gfrom=" + encodeURIComponent(incomingAddress) + "&gto=" + encodeURIComponent(destinationAddress) + "&phone=" + encodeURIComponent(incomingPhone) + "#index", "yandex", 'height=600,width=1024');
			if(window.focus) {
				newWindow.focus();
			}
			statisticsManager.addStatisticsRecord(new Date().getTime(), "existing", incomingAddress, destinationAddress, incomingPhone);

			var operatorName = strip_tags($("div[ng-if='currentUser.isLoggedIn()']").find("span[ng-click='topPanelMenu.toggle($event)']").html());
			console.log("operatorName = " + operatorName);
			hivetaxi_sendStatistics(operatorName, "existing", incomingAddress, destinationAddress, incomingPhone, function(msg) {
				console.log("send statistics result");
				console.log(msg);
			});
			
			//отменяем заказ
			$(".right-panel").find(".-cancel")[0].click();
			setTimeout(onExistingOrderCancelPressed, 1500);
		});
	}
}


function sendKey(keyCode) {
//	var keyboardEvent = document.createEvent("KeyboardEvent");
//	var initMethod = typeof keyboardEvent.initKeyboardEvent !== 'undefined' ? "initKeyboardEvent" : "initKeyEvent";
//
//
//	keyboardEvent[initMethod](
//	                   "keydown", // event type : keydown, keyup, keypress
//	                    true, // bubbles
//	                    true, // cancelable
//	                    window, // viewArg: should be window
//	                    false, // ctrlKeyArg
//	                    false, // altKeyArg
//	                    false, // shiftKeyArg
//	                    false, // metaKeyArg
//	                    keyCode, // keyCodeArg : unsigned long the virtual key code, else 0
//	                    0 // charCodeArgs : unsigned long the Unicode character associated with the depressed key, else 0
//	);
//	document.dispatchEvent(keyboardEvent);
	var e = jQuery.Event("keydown");

	// e.which is used to set the keycode
//	e.which = 38; // it is up
	e.which = keyCode; // it is down
	$("body").trigger(e);
}


function onExistingOrderCancelPressed() {
	sendKey(40);
	sendKey(40);
	sendKey(40);
	sendKey(40);
	sendKey(40);
	sendKey(40);
	sendKey(40);
	sendKey(13);
//	$($(".popup").find(".select-wrapper").find(".select-list").find("li").get(7))[0].click();
	$(".popup").find(".s-textarea").val("Заказ передан в Яндекс");
	setTimeout(onCancelReasonSelected, 500);
}


function onCancelReasonSelected() {
	$(".popup").find(".popup_footer-right-btns").find(".-normal").removeAttr("disabled");
	$(".popup").find(".popup_footer-right-btns").find(".-normal")[0].click();
}


function isExistingOrderPreorder() {
	var foundNotPreorder = false;
	$(".right-panel").find(".md-normal").each(function() {
		if($(this).html() == "На ближайшее время") {
			foundNotPreorder = true;
			return;
		}
	});
	return !foundNotPreorder;
}


function popupCheckPreorder() {
	var item = $("order-modal").find(".order-type").find(".multi-switcher_item").get(0);
	if($(item).hasClass("active")) {
		return false;
	}
	return true;
}

/**
 * Проверяет, что задано одно конечное местоположение
 * @returns true если задан один конечный адрес
 */
function popupIsMultipleDestination() {
	var length = $("order-modal").find(".address-to_wrapper").find(".address_row").length;
//	console.log("popupIsMultipleDestination - l1 = " + length);
	if(length > 2) {
		return true;
	}
	var element = $("order-modal").find(".address-to_wrapper").find(".address_row").get(1);
	length = $(element).find(".h-input_box").length; 
//	console.log("popupIsMultipleDestination - l2 = " + length);
	return  length > 1;
}

function popupGetDestinationAddress() {
	var address = "";
	var element = $("order-modal").find(".address-to_wrapper").find(".address_row").get(0);
	$(element).find(".h-input_box").each(function() {
		var value = $(this).html();
//		console.log(value);
		if(value.indexOf("...") != -1) {
			//если строка обрезана, то надо брать из подсказки выше, там полная строка
			var title = $(this).parent().attr("title");
			var startIndex = title.indexOf("[");
			var endIndex = title.indexOf("]");
			value = title.substring(startIndex + 1, endIndex);
		}
//		console.log(value);
		if(!address == "") {
			address = address + ", ";
		}
		address = address + value;
	});
	return address;
}


function popupGetIcomingAddress() {
	var address = "";
	$("order-modal").find(".incoming-address").find(".h-input_box").each(function() {
		var value = $(this).html();
//		console.log(value);
		if(value.indexOf("...") != -1) {
			//если строка обрезана, то надо брать из подсказки выше, там полная строка
			var title = $(this).parent().attr("title");
			var startIndex = title.indexOf("[");
			var endIndex = title.indexOf("]");
			value = title.substring(startIndex + 1, endIndex);
		}
//		console.log(value);
		if(!address == "") {
			address = address + ", ";
		}
		address = address + value;
	});
	return address;
}


function popupGetIncomingPhone() {
	var number = null;
	$("order-modal").find(".popup_aside-section").find(".s-label").each(function() {
		if($(this).html() == "ВХОДЯЩИЙ НОМЕР") {
			var parent = $(this).parent();
			number = parent.find(".md-bold").html();
//			console.log("row number: " + number);
			if(!number) {
				return;
			}
			number = number.replace( /[^0-9]/g, '' );
//			console.log("after replace: " + number);
			if(number.startsWith("8")) {
				number = "7" + number.substring(1, number.length);
			}
			number = "+" + number;
		}
	});
	return number;
}


function popupAddYandexButton(parent) {
	var button = document.createElement("button");
	$(button).attr("id", "btnSendToYandex");
	$(button).addClass("s-button");
	$(button).addClass("-std");
	$(button).addClass("-normal");
	var span = document.createElement("span");
	$(span).html("Открыть в Яндексе");
	button.appendChild(span);
	var eElement = parent.get(0);
	eElement.insertBefore(button, eElement.firstChild);
//	parent.get(0).appendChild(button);
	$(button).click(function() {
		var incomingPhone = popupGetIncomingPhone();
		console.log("incomingPhone = " + incomingPhone);
		var incomingAddress = popupGetIcomingAddress();
		console.log("incomingAddress = " + incomingAddress);
		var destinationAddress = popupGetDestinationAddress();
		console.log("destinationAddress = " + destinationAddress);
		newWindow = window.open("https://taxi.yandex.ru/partner-orders/?gfrom=" + encodeURIComponent(incomingAddress) + "&gto=" + encodeURIComponent(destinationAddress) + "&phone=" + encodeURIComponent(incomingPhone) + "#index", "yandex", 'height=600,width=1024');
		if(window.focus) {
			newWindow.focus();
		}
		statisticsManager.addStatisticsRecord(new Date().getTime(), "new", incomingAddress, destinationAddress, incomingPhone);

		var operatorName = strip_tags($("div[ng-if='currentUser.isLoggedIn()']").find("span[ng-click='topPanelMenu.toggle($event)']").html());
		console.log("operatorName = " + operatorName);
		hivetaxi_sendStatistics(operatorName, "new", incomingAddress, destinationAddress, incomingPhone, function(msg) {
			console.log("send statistics result");
			console.log(msg);
		});

		//закрываем попап с заказом
		$("order-modal").find(".-cancel")[0].click();
	});
}


function hivetaxi_sendStatistics(operator_name, order_status, incoming_address, destination_address, incoming_phone, resultCallback) {
	resultCallbackFunction = resultCallback;
	var data = {
		operator_name: operator_name,
		order_status: order_status,
		incoming_address: incoming_address,
		destination_address: destination_address,
		incoming_phone: incoming_phone,
	};
	console.log("hivetaxi_sendStatistics");
	$.ajax({
		url: API_HOST + '/statistics_manager/add_statistics_record/',
		type: 'POST',
		data: JSON.stringify(data),
		contentType: 'application/json; charset=utf-8',
		dataType: 'json',
		success: function(msg) {
			console.log(msg);
			if(resultCallbackFunction) {
				resultCallbackFunction(msg);
			}
		}
	});				
}
