function StatisticsRecord(id, timestamp, orderStatus, incomingAddress, destinationAddress, incomingPhone) {
	this.id = id;
	this.timestamp = timestamp;
	this.orderStatus = orderStatus;
	this.incomingAddress = incomingAddress;
	this.destinationAddress = destinationAddress;
	this.incomingPhone = incomingPhone,
	
	
	this.getId = function() {
		return this.id;
	};
	
	
	this.getTimestamp = function() {
		return this.timestamp;
	};
	
	
	this.getOrderStatus = function() {
		return this.orderStatus;
	};
	
	
	this.getIncomingAddress = function() {
		return this.incomingAddress;
	};
	
	
	this.getDestinationAddress = function() {
		return this.destinationAddress;
	};
	

	this.getIncomingPhone = function() {
		return this.incomingPhone;
	};
	
	
	this.serialize = function() {
		return JSON.stringify({
			id: this.id,
			timestamp: this.timestamp,
			orderStatus: this.orderStatus,
			incomingAddress: this.incomingAddress,
			destinationAddress: this.destinationAddress,
			incomingPhone: this.incomingPhone,
		});
	};
}