function StatisticsManager() {
	this.lastId = localStorage.getItem("hivetaxi_statistics_last_id");
	if(!this.lastId) {
		this.lastId = 1;
	}
	
	
	this.addStatisticsRecord = function(timestamp, orderStatus, incomingAddress, destinationAddress, incomingPhone) {
		var statisticsRecord = new StatisticsRecord(this.lastId, timestamp, orderStatus, incomingAddress, destinationAddress, incomingPhone);
		localStorage.setItem("hivetaxi_statistics_record_" + this.lastId, statisticsRecord.serialize());
		this.lastId++;
		localStorage.setItem("hivetaxi_statistics_last_id", this.lastId);
	};
	
	this.getStatisticsRecord = function(id) {
		var data = localStorage.getItem("hivetaxi_statistics_record_" + id);
		if(!data) {
			return null;
		}
//		console.log(data);
		var record = JSON.parse(data);
//		console.log(record);
		return new StatisticsRecord(record.id, record.timestamp, record.orderStatus, record.incomingAddress, record.destinationAddress, record.incomingPhone);
	};
	
	
	this.getStatistics = function() {
		var data = new Array();
		for(var i=0; i<this.lastId; ++i) {
			var record = this.getStatisticsRecord(i);
			if(record) {
				data.push(record);
			}
		}
		return data;
	};
}